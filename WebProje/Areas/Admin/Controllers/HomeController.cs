﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Areas.Admin.Models.Attributes;

namespace WebProje.Areas.Admin.Controllers
{
    [LoginControl] // Giriş yapılıp yapılmadığını kontrol ediyor.
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}