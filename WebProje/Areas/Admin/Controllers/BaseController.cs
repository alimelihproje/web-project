﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Areas.Admin.Models.Attributes;
using WebProje.Models.ORM.Context;

namespace WebProje.Areas.Admin.Controllers
{
    [LoginControl]
    public class BaseController : Controller
    {
        public DatabaseContext db;

        public BaseController()
        {
            //ViewBag.User = HttpContext.User.Identity.Name;
            db = new DatabaseContext();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }
    }
}