﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Areas.Admin.Models.DTO;
using System.Web.Security;
using WebProje.Models.ORM.Context;

namespace WebProje.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {

        private DatabaseContext db = new DatabaseContext();
        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginModel model)
        {
            if(ModelState.IsValid)
            {
                if (db.AdminUsers.Any(x => x.EMail == model.EMail && x.Password == model.Password && x.IsDeleted == false))
                {
                    FormsAuthentication.SetAuthCookie(model.EMail, true); // mail adresi şifrelenmiş bir şekilde cookiede tutuluyor.
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return View();
                }
                
            }
            else
            {
                return View();
            }
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
    }
}