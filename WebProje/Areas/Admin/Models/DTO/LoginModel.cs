﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebProje.Areas.Admin.Models.DTO
{
    public class LoginModel
    {
        [Required(ErrorMessage ="Lütfen e-posta adresini giriniz.")]
        [EmailAddress(ErrorMessage ="Düzgün bir e-posta adresi giriniz.")]
        public string EMail { get; set; }

        [Required(ErrorMessage ="Lütfen şifrenizi giriniz.")]
        public string Password { get; set; }
    }
}