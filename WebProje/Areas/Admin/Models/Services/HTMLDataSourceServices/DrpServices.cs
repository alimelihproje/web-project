﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Models.ORM.Context;

namespace WebProje.Areas.Admin.Models.Services.HTMLDataSourceServices
{
    public class DrpServices
    {
        public static IEnumerable<SelectListItem> getDrpCategories()
        {
            using (DatabaseContext db = new DatabaseContext())
            {
                IEnumerable<SelectListItem> drpcategories = db.Categories.Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.ID.ToString()
                }).ToList();

                return drpcategories;
            }

           
        }
    }
}