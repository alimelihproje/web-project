﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProje.Areas.Admin.Models.Types
{
    public class FormMessages
    {
        public static string success = "İşlem başarıyla gerçekleştirildi.";
        public static string required = "Lütfen belirtilen alanları doldurunuz.";
    }
}