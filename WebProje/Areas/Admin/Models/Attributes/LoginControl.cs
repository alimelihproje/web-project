﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebProje.Areas.Admin.Models.Attributes
{
    public class LoginControl : ActionFilterAttribute
    {
        //Attribute girildiği zaman alttaki metod calısacak
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if(!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.HttpContext.Response.Redirect("/Admin/Login/Index");
            }
        }
    }
}