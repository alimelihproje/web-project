﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Models.ORM.Context;

namespace WebProje.Controllers
{
    public class SiteBaseController : Controller
    {
        public DatabaseContext db;

        // GET: Base
        public SiteBaseController()
        {
            db = new DatabaseContext();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }
    }
}