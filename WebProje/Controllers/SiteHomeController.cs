﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebProje.Areas.Admin.Models.DTO;
using WebProje.Models.VM;
using PagedList;
using PagedList.Mvc;
using System.Threading;
using System.Globalization;

namespace WebProje.Controllers
{
    public class SiteHomeController : SiteBaseController
    {
        // GET: SiteHome
        public ActionResult Index(int page=1)
        {
            var model = db.BlogPosts.Where(x => x.IsDeleted == false).OrderBy(x => x.AddDate).Select(x => new SiteBlogPostVM()
            {
                Title = x.Title,
                Content = x.Content,
                PostImagePath = x.ImagePath,
                Category = x.Category.Name,
                BlogPostID = x.ID,
                AddDate = x.AddDate,
                CategoryID = x.CategoryID

            }).ToList().ToPagedList(page, 2);

            

            return View(model);
        }

        public ActionResult KategoriPartial()
        {
            List<CategoryVM> model = db.Categories.Where(x => x.IsDeleted == false).OrderBy(x => x.AddDate).Select(x => new CategoryVM()
            {
                Name = x.Name,
                Description = x.Description,
                ID = x.ID
            }).ToList(); // KAtegori objesi CategoryVM'ye çevrildi.

            return View(model);

        }

        public ActionResult ByCategory(int? id,int page=1)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var model = db.BlogPosts.Where(x => x.IsDeleted == false && x.CategoryID == id).OrderBy(x => x.AddDate).Select(x => new SiteBlogPostVM()
            {
                Title = x.Title,
                Content = x.Content,
                PostImagePath = x.ImagePath,
                Category = x.Category.Name,
                BlogPostID = x.ID,
                AddDate = x.AddDate,
                CategoryID = x.CategoryID

            }).ToList().ToPagedList(page,2);

            return View("Index", model);
        }


        public ActionResult Change(String LanguageAbbrevation)
        {
            if (LanguageAbbrevation != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(LanguageAbbrevation);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(LanguageAbbrevation);
            }

            HttpCookie cookie = new HttpCookie("SiteHome");
            cookie.Value = LanguageAbbrevation;
            Response.Cookies.Add(cookie);

            return RedirectToAction("Index");
        }
    }
}