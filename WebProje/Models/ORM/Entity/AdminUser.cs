﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebProje.Models.ORM.Entity
{
    public class AdminUser : BaseEntity
    {
        [StringLength(30)]
        public string Name { get; set; }

        [StringLength(40)]
        public string Surname { get; set; }

        [Required]
        [StringLength(100)]
        public string EMail { get; set; }

        [Required]
        public string Password { get; set; }
    }
}