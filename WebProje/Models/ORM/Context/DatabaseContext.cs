﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebProje.Models.ORM.Entity;

namespace WebProje.Models.ORM.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
            Database.Connection.ConnectionString = "Server=DESKTOP-UUQMS5R;Database=WebProjeDB;Trusted_connection=true;";

        }


        //Tablolardaki çoğul ekini kaldırmak için
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<AdminUser> AdminUsers { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<SiteMenu> SiteMenus { get; set; }
        public DbSet<BlogPostComment> BlogPostComments { get; set; }


    }
}