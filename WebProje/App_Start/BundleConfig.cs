﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace WebProje.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/admin/layoutscripts").Include(
                "~/Scripts/jquery.min.js",
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/jquery.slimscroll.min.js",
                "~/Scripts/fastclick.js",
                "~/Scripts/adminlte.min.js",
                "~/Scripts/demo.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/Site/jquery-1.11.0.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/Site/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/others").Include(
             "~/Scripts/Site/jquery.cookie.js",
             "~/Scripts/Site/waypoints.min.js",
             "~/Scripts/Site/jquery.counterup.min.js",
             "~/Scripts/Site/jquery.parallax-1.1.3.js",
             "~/Scripts/Site/front.js",
             "~/Scripts/Site/owl.carousel.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
            "~/Content/css/roboto.css",
            "~/Content/css/bootstrap.min.css",
            "~/Content/css/font-awesome.css",
            "~/Content/css/animate.css",
            "~/Content/css/style.blue.css"));

            bundles.Add(new StyleBundle("~/Content/owlcss").Include(
            "~/Content/css/owl.carousel.css",
            "~/Content/css/owl.theme.css"));

        }
    }
}